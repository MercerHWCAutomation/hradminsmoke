var seleniumServer = require('selenium-server');
var chromedriver = require('chromedriver');
var data = require('./TestResources/GlobalTestData');
require('nightwatch-cucumber')({
    supportFiles: ['./utils/TestExecListener.js'],
    stepTimeout:900000,
    defaultTimeoutInterval:900000
})

var fnRandomNumber = function (no_of_digits) {
    var max = 9, min = 0, str="", i;
    for(i=0; i < no_of_digits; i++){
        str = str + Math.floor(Math.random() * (max - min) + min);
    }
    return str;
};

var RandomPort =fnRandomNumber(4);

module.exports = {
  output_folder: 'reports',
  custom_commands_path: '',
  custom_assertions_path: '',
  page_objects_path : "repository",
  live_output: false,
  disable_colors: false,
  selenium: {
    start_process: true,
    server_path: seleniumServer.path,
    host: '127.0.0.1',
      port: RandomPort,
    cli_args: {
        'webdrover.chrome.driver': chromedriver.path,
        'webdriver.ie.driver': 'C:\\Users\\subhajit-chakraborty\\Documents\\IEDriverServer.exe',
        'webdriver.firefox.profile': ''
    }
  },

  test_settings: {
    default : {
      launch_url: "http://localhost",
      page_objects_path : "repository",
      selenium_host: "127.0.0.1",
        selenium_port: RandomPort,
      silent : true,
      disable_colors: false,
        screenshots: {
            enabled: false,
            on_failure: false,
            on_error: false,
            path: 'screenshots'
        },

        desiredCapabilities: {
            browserName: data.BrowserInTest,
        javascriptEnabled : true,
        acceptSslCerts : true
      }
    },
  }
}

